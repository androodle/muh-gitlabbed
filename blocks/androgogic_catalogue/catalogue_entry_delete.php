<?php

/** 
 * Androgogic Catalogue Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the catalogue_entries
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('andro_catalogue_entry',array('id'=>$id));
$DB->delete_records('andro_catalogue_entlocation',array('catalogue_entry_id'=>$id));
$DB->delete_records('andro_catalogue_course',array('catalogue_entry_id'=>$id));
$DB->delete_records('andro_catalogue_program',array('catalogue_entry_id'=>$id));
$DB->delete_records('andro_catalogue_organisation',array('catalogue_entry_id'=>$id));
$DB->delete_records('andro_catalogue_position',array('catalogue_entry_id'=>$id));
$DB->delete_records('andro_catalogue_competency',array('catalogue_entry_id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_catalogue'), 'notifysuccess');

?>
