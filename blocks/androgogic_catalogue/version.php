<?php
/** 
 * Androgogic Catalogue Block: Version
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$plugin->version = 2014120900;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0

// End of blocks/androgogic_catalogue/version.php
