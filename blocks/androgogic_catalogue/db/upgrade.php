<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

function xmldb_block_androgogic_catalogue_upgrade($oldversion) {
    global $DB, $CFG;

    $dbman = $DB->get_manager();

    if ($oldversion < 2014120900) {

        $table = new xmldb_table('androgogic_catalogue_entries');
        $dbman->rename_table($table, 'andro_catalogue_entry');
        
         $table = new xmldb_table('androgogic_catalogue_entry_courses');
        $dbman->rename_table($table, 'andro_catalogue_course');
        
         $table = new xmldb_table('androgogic_catalogue_entry_locations');
        $dbman->rename_table($table, 'andro_catalogue_entlocation');
        
         $table = new xmldb_table('androgogic_catalogue_entry_organisations');
        $dbman->rename_table($table, 'andro_catalogue_organisation');
        
         $table = new xmldb_table('androgogic_catalogue_entry_positions');
        $dbman->rename_table($table, 'andro_catalogue_position');
        
         $table = new xmldb_table('androgogic_catalogue_entry_programs');
        $dbman->rename_table($table, 'andro_catalogue_program');
        
         $table = new xmldb_table('androgogic_catalogue_entry_competencies');
        $dbman->rename_table($table, 'andro_catalogue_competency');
        
         $table = new xmldb_table('androgogic_catalogue_locations');
        $dbman->rename_table($table, 'andro_catalogue_location');
        upgrade_plugin_savepoint(true, 2014120900, 'block', 'androgogic_catalogue');

    }

    return true;
}
