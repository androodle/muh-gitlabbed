<?php
/** 
 * Androgogic Catalogue Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/
function user_can_see_catalogue_entry($catalogue_entry,$user_pos_org_data){
    global $USER,$DB;
    $user_can_see = true;
    // any org entries for this cat_entry?
    $q = "SELECT o.* FROM mdl_andro_catalogue_organisation co 
        INNER JOIN mdl_org o ON co.organisation_id = o.id 
        WHERE catalogue_entry_id = $catalogue_entry->id";
    $cat_org_result = $DB->get_records_sql($q);
    
    // any pos entries for this cat_entry?
    $q = "SELECT o.* FROM mdl_andro_catalogue_position co 
        INNER JOIN mdl_pos o ON co.position_id = o.id 
        WHERE catalogue_entry_id = $catalogue_entry->id";
    $cat_pos_result = $DB->get_records_sql($q);
    
    if(!$cat_org_result && !$cat_pos_result){
        //no filters on this catalogue entry, so nothing else to do here
        return true;
    }
    else{
        $passed_org_test = false;
        $passed_pos_test = false;
        if($cat_org_result){
           if(!isset($user_pos_org_data->user_org_result)){
                //fail: no user org, but we do have cat org, so let's short circuit this
                return false;
            }
            foreach ($user_pos_org_data->user_org_result as $user_org) {
                // are any of the cat org paths upstream from the user org path?
                foreach ($cat_org_result as $cat_org) {
                    //close the paths, in case of false positives
                    if(stristr($user_org->path.'/',$cat_org->path.'/')){
                        //in other words, does the user belong to an organisation that either is the same as the cat org, or is among the descendants of it?
                        $passed_org_test = true;
                    }
                }
            }  
        }
        else{
            $passed_org_test = true;
        }
        if($cat_pos_result){
            if(!isset($user_pos_org_data->user_pos_result)){
                //fail: no user pos, but we do have cat pos, so let's short circuit this
                return false;
            }
            foreach ($user_pos_org_data->user_pos_result as $user_pos) {
                //does the user pos path match any of the cat_pos paths?
                foreach ($cat_pos_result as $cat_pos) {
                    if($user_pos->path == $cat_pos->path){
                        $passed_pos_test = true;
                    }
                    elseif(stristr($cat_pos->path,$user_pos->path.'/')){
                        //in other words, does the user hold a higher position within the hierarchy than the specified catalogue position?
                        $passed_pos_test = true;
                    }
                }
            }            
        }
        else{
            $passed_pos_test = true;
        }
    }    
    //if we got down to here then we should have the results of the 2 tests. if both are true then the user can see, if not, not
    if($passed_org_test && $passed_pos_test){
        $user_can_see = true;
    }
    else{
        $user_can_see = false;
    }
    return $user_can_see;
}
function get_user_pos_org_data(){
    global $USER,$DB;
    //get org data for the user
    $q = "SELECT o.* FROM mdl_pos_assignment pa
        INNER JOIN mdl_org o ON pa.organisationid = o.id
        WHERE  userid = {$USER->id} AND TYPE IN (1,2)";
    $user_org_result = $DB->get_records_sql($q);
    
    //get pos data for the user
    $q = "SELECT p.* FROM mdl_pos_assignment pa
        INNER JOIN mdl_pos p ON pa.positionid = p.id
        WHERE  userid = {$USER->id} AND TYPE IN (1,2)";
    $user_pos_result = $DB->get_records_sql($q);
    
    $return = new stdClass();
    $return->user_org_result = $user_org_result;
    $return->user_pos_result = $user_pos_result;
    return $return;
}


// End of blocks/androgogic_catalogue/lib.php
