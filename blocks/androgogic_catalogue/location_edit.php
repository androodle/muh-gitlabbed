<?php

/** 
 * Androgogic Catalogue Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the locations
 *
 **/

global $OUTPUT;
require_once('location_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select a.*  
from mdl_andro_catalogue_location a 
where a.id = $id ";
$location = $DB->get_record_sql($q);
$mform = new location_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('andro_catalogue_location',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_catalogue'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('location_edit', 'block_androgogic_catalogue'));
$mform->display();
}

?>
