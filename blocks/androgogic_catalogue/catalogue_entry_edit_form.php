<?php

/** 
 * Androgogic Catalogue Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class catalogue_entry_edit_form extends moodleform {
protected $catalogue_entry;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_andro_catalogue_entry a 
where a.id = {$_REQUEST['id']} ";
$catalogue_entry = $DB->get_record_sql($q);
}
else{
$catalogue_entry = $this->_customdata['$catalogue_entry']; // this contains the data of this form
}
$tab = 'catalogue_entry_new'; // from whence we were called
if (!empty($catalogue_entry->id)) {
$tab = 'catalogue_entry_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_catalogue'), array('size'=>50));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');

//end_date
$mform->addElement('date_selector', 'end_date', get_string('end_date','block_androgogic_catalogue'));

//description
$editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
$mform->addElement('editor', 'description', get_string('description','block_androgogic_catalogue'), null, $editoroptions);
$mform->setType('description', PARAM_CLEANHTML);

//public
$mform->addElement('selectyesno', 'public', get_string('public','block_androgogic_catalogue'));

//location_id
$options = $DB->get_records_menu('andro_catalogue_location',array(),'name','id,name');
$select = $mform->addElement('select', 'location_id', get_string('location','block_androgogic_catalogue'), $options,array('size'=>10));
$select->setMultiple(true);


//course_id
$options = $DB->get_records_menu('course',array(),'fullname','id,fullname');
$select = $mform->addElement('searchableselector', 'course_id', get_string('course','block_androgogic_catalogue'), $options,array('size'=>10));
$select->setMultiple(true);


//program_id
$options = $DB->get_records_menu('prog',array(),'fullname','id,fullname');
$select = $mform->addElement('select', 'program_id', get_string('program','block_androgogic_catalogue'), $options,array('size'=>10));
$select->setMultiple(true);


//organisation_id
$options = $DB->get_records_menu('org',array(),'fullname','id,fullname');
$select = $mform->addElement('select', 'organisation_id', get_string('organisation','block_androgogic_catalogue'), $options,array('size'=>10));
$select->setMultiple(true);


//position_id
$options = $DB->get_records_menu('pos',array(),'fullname','id,fullname');
$select = $mform->addElement('select', 'position_id', get_string('position','block_androgogic_catalogue'), $options,array('size'=>10));
$select->setMultiple(true);


//competency_id
$options = $DB->get_records_menu('comp',array(),'fullname','id,fullname');
$select = $mform->addElement('select', 'competency_id', get_string('competency','block_androgogic_catalogue'), $options,array('size'=>10));
$select->setMultiple(true);

//set values if we are in edit mode
if (!empty($catalogue_entry->id) && isset($_GET['id'])) {
$mform->setConstant('name', $catalogue_entry->name);
$mform->setConstant('end_date', strtotime($catalogue_entry->end_date));
$description['text'] = $catalogue_entry->description;
$description['format'] = 1;
$mform->setDefault('description', $description);
$mform->setConstant('public', $catalogue_entry->public);
//many to many columns
$result = $DB->get_records('andro_catalogue_entlocation',array('catalogue_entry_id'=>$catalogue_entry->id));
$location_ids = array();
foreach($result as $row){
$location_ids[] = $row->location_id;
}
if(count($location_ids) > 0){
$mform->setConstant('location_id', $location_ids);
}
$result = $DB->get_records('andro_catalogue_course',array('catalogue_entry_id'=>$catalogue_entry->id));
$course_ids = array();
foreach($result as $row){
$course_ids[] = $row->course_id;
}
if(count($course_ids) > 0){
$mform->setConstant('course_id', $course_ids);
}
$result = $DB->get_records('andro_catalogue_program',array('catalogue_entry_id'=>$catalogue_entry->id));
$program_ids = array();
foreach($result as $row){
$program_ids[] = $row->program_id;
}
if(count($program_ids) > 0){
$mform->setConstant('program_id', $program_ids);
}
$result = $DB->get_records('andro_catalogue_organisation',array('catalogue_entry_id'=>$catalogue_entry->id));
$organisation_ids = array();
foreach($result as $row){
$organisation_ids[] = $row->organisation_id;
}
if(count($organisation_ids) > 0){
$mform->setConstant('organisation_id', $organisation_ids);
}
$result = $DB->get_records('andro_catalogue_position',array('catalogue_entry_id'=>$catalogue_entry->id));
$position_ids = array();
foreach($result as $row){
$position_ids[] = $row->position_id;
}
if(count($position_ids) > 0){
$mform->setConstant('position_id', $position_ids);
}
$result = $DB->get_records('andro_catalogue_competency',array('catalogue_entry_id'=>$catalogue_entry->id));
$competency_ids = array();
foreach($result as $row){
$competency_ids[] = $row->competency_id;
}
if(count($competency_ids) > 0){
$mform->setConstant('competency_id', $competency_ids);
}
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
