<?php

/** 
 * Androgogic Catalogue Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new catalogue_entry
 *
 **/

global $OUTPUT;
require_once('catalogue_entry_edit_form.php');
$mform = new catalogue_entry_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$data->end_date = date('Y-m-d',$data->end_date);
$data->description = format_text($data->description['text'], $data->description['format']);
$newid = $DB->insert_record('andro_catalogue_entry',$data);
//many to many relationship: andro_catalogue_entlocation
if(isset($data->location_id)){
foreach($data->location_id as $location_id){
$insert = new stdClass();
$insert->catalogue_entry_id = $newid;
$insert->location_id = $location_id;
$DB->insert_record('andro_catalogue_entlocation', $insert);
}
}
//many to many relationship: andro_catalogue_course
if(isset($data->course_id)){
foreach($data->course_id as $course_id){
$insert = new stdClass();
$insert->catalogue_entry_id = $newid;
$insert->course_id = $course_id;
$DB->insert_record('andro_catalogue_course', $insert);
}
}
//many to many relationship: andro_catalogue_program
if(isset($data->program_id)){
foreach($data->program_id as $program_id){
$insert = new stdClass();
$insert->catalogue_entry_id = $newid;
$insert->program_id = $program_id;
$DB->insert_record('andro_catalogue_program', $insert);
}
}
//many to many relationship: andro_catalogue_organisation
if(isset($data->organisation_id)){
foreach($data->organisation_id as $organisation_id){
$insert = new stdClass();
$insert->catalogue_entry_id = $newid;
$insert->organisation_id = $organisation_id;
$DB->insert_record('andro_catalogue_organisation', $insert);
}
}
//many to many relationship: andro_catalogue_position
if(isset($data->position_id)){
foreach($data->position_id as $position_id){
$insert = new stdClass();
$insert->catalogue_entry_id = $newid;
$insert->position_id = $position_id;
$DB->insert_record('andro_catalogue_position', $insert);
}
}
//many to many relationship: andro_catalogue_competency
if(isset($data->competency_id)){
foreach($data->competency_id as $competency_id){
$insert = new stdClass();
$insert->catalogue_entry_id = $newid;
$insert->competency_id = $competency_id;
$DB->insert_record('andro_catalogue_competency', $insert);
}
}
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_catalogue'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('catalogue_entry_new', 'block_androgogic_catalogue'));
$mform->display();
}

?>
