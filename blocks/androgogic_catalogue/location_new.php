<?php

/** 
 * Androgogic Catalogue Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new location
 *
 **/

global $OUTPUT;
require_once('location_edit_form.php');
$mform = new location_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$newid = $DB->insert_record('andro_catalogue_location',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_catalogue'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('location_new', 'block_androgogic_catalogue'));
$mform->display();
}

?>
