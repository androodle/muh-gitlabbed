<?php

/**
 * Androgogic Support Block: Install DB scripts
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     03/07/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_block_androgogic_support_install() {
    global $DB;
    $dbman = $DB->get_manager();
    $result = true;
// create tables for the block
    if (!$dbman->table_exists('androgogic_faq')) {
        $table = new xmldb_table('androgogic_faq');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('question', XMLDB_TYPE_TEXT, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_field('answer', XMLDB_TYPE_TEXT, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_field('listing_order', XMLDB_TYPE_INTEGER, '6', null, XMLDB_NOTNULL, null, null);
        $table->add_field('created_by', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('date_created', XMLDB_TYPE_DATETIME, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_field('modified_by', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('date_modified', XMLDB_TYPE_DATETIME, 'null', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
    }
    if (!$dbman->table_exists('androgogic_server_status')) {
        $table = new xmldb_table('androgogic_server_status');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('summary', XMLDB_TYPE_TEXT, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_field('active', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, null);
        $table->add_field('listing_order', XMLDB_TYPE_INTEGER, '6', null, XMLDB_NOTNULL, null, null);
        $table->add_field('created_by', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('date_created', XMLDB_TYPE_DATETIME, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_field('modified_by', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('date_modified', XMLDB_TYPE_DATETIME, 'null', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
    }
    if (!$dbman->table_exists('androgogic_support_log')) {
        $table = new xmldb_table('androgogic_support_log');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('first_name', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('last_name', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('contact_number', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('problem_description', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('steps_to_reproduce', XMLDB_TYPE_TEXT, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_field('uploaded_file_id', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);
        $table->add_field('site_name', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('page_before_support', XMLDB_TYPE_CHAR, '500', null, XMLDB_NOTNULL, null, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('date_created', XMLDB_TYPE_DATETIME, 'null', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
    }
    return $result;
}
