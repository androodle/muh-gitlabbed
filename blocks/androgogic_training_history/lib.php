<?php

/** 
 * Androgogic Training History Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

function block_androgogic_training_history_cron($source='cron'){
    global $CFG,$DB;
    $starthour = date('G');
    echo '<pre>the starthour:';
    var_dump($starthour);
    echo 'the androgogic_training_history run cron hours: ';
    var_dump($CFG->block_androgogic_training_history_run_cron_hours);
    echo '</pre>';
   if(isset($CFG->block_androgogic_training_history_run_cron_hours) && in_array($starthour,explode(';',$CFG->block_androgogic_training_history_run_cron_hours))){
        mtrace("\n\n");
         mtrace("Running cron for androgogic_training_history"."<br><br>");
        $time = date("d-m-Y h-i-s",time());
        echo $time . '<br/>';
        // REPLACE 'this_function' WITH THE ONES THAT WE WANT TO CALL
        echo '<br>before this_function:<br>';
        flush();
        echo _this_function($source);
        $time = date("d-m-Y h-i-s",time());
        echo 'Finish time' . $time . '<br/>';
    }
    else
    {
        mtrace("\n\n");
        mtrace("not one of the run cron hours for androgogic_training_history");
     }
}
function block_androgogic_training_history_save_upload_over_existing($form_data,$block_name,$table_name,$foreign_key_name){
    global $DB;
   $form_elements = $form_data->_form->_elements;
   //reformat for ease of use
   $data = new stdClass();
   foreach ($form_elements as $element) {
//       echo '<pre> the $element:';
//       var_dump($element);
//       echo '</pre>';
       if(isset($element->_attributes['name']) && isset($element->_attributes['value'])){
            $data->{$element->_attributes['name']} = $element->_attributes['value'];
       }
       else{
           //echo 'missing attribute for this element: ' . print_r($element);
       }
   }
//   echo '<pre> the $data:';
//   var_dump($data);
//   echo '</pre>';
   //change the reference to the itemid in the block table
   $sql = "update mdl_$table_name set $foreign_key_name = " . $data->itemid . " where id = ". $data->objectid;
   $DB->execute($sql);
   //change the component and filearea of the file entry
   $sql = "UPDATE mdl_files
        SET filearea = 'content',component='$block_name'
        WHERE itemid = '$data->itemid'";
   $DB->execute($sql);
   
}
function block_androgogic_training_history_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload){
    global $DB;
    $sql = 'select * from mdl_files 
            where itemid = '.$args[0].' 
                and filename = \'' . $args[1] .'\'
                and contextid = '. $context->id . ' 
                and filearea = \'' . $filearea . '\'';
    $file = $DB->get_record_sql($sql);
    if(!$file){
        send_file_not_found();
    }
    else{
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }
    $forcedownload = true;    

    session_get_instance()->write_close();
    send_stored_file($stored_file, 60*60, 0, $forcedownload);
}
/**
 * the user (or an admin) has put in a new training history record. let their manager know this by email
 * @param object $training_history_record
 */
function block_androgogic_training_history_advise_manager($training_history_record){
    global $DB,$CFG,$OUTPUT;
    $th_user = $DB->get_record('user',array('id'=>$training_history_record->user_id));
    $q = "SELECT u.* FROM mdl_pos_assignment pa inner join mdl_user u on pa.managerid = u.id where pa.userid = $training_history_record->user_id";
    $manager = $DB->get_record_sql($q);
    if($manager){
        $subject = get_string('email_to_manager_subject','block_androgogic_training_history');
        $a->url = $CFG->wwwroot . '/blocks/androgogic_training_history/index.php?tab=training_history_edit&id='.$training_history_record->id;
        $body = get_string('email_to_manager_body','block_androgogic_training_history',$a);
        email_to_user($manager, $th_user, $subject, $body, $body);
    }
    else{
        echo $OUTPUT->notification(get_string('userhasnomanager','block_androgogic_training_history'), 'notifyfailure');
    }
}
/**
 * the manager has approved the training history record. let the user know this by email
 * @param object $training_history_record
 */
function block_androgogic_training_history_advise_user($training_history_record){
    global $DB,$CFG,$OUTPUT;
    $th_user = $DB->get_record('user',array('id'=>$training_history_record->user_id));
    $q = "SELECT u.* FROM mdl_pos_assignment pa inner join mdl_user u on pa.managerid = u.id where pa.userid = $training_history_record->user_id";
    $manager = $DB->get_record_sql($q);
    if($manager){
        $subject = get_string('email_to_user_subject','block_androgogic_training_history');
        $a->url = $CFG->wwwroot . '/blocks/androgogic_training_history/index.php?tab=training_history_edit&id='.$training_history_record->id;
        $body = get_string('email_to_user_body','block_androgogic_training_history',$a);
        email_to_user($th_user,$manager, $subject, $body, $body);
    }
    else{
        echo $OUTPUT->notification(get_string('userhasnomanager','block_androgogic_training_history'), 'notifyfailure');
    }
}
/**
 * is the user a manager of others?
 */
function block_androgogic_training_history_is_manager(){
    global $USER,$DB;
    $is_manager = false;
    if($DB->get_records('pos_assignment',array('managerid'=>$USER->id))){
        $is_manager = true;
    }
    return $is_manager;
}