<?php

/**
 * Androgogic Training History Block: CPD Report
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Report on user's CPD points
 *
 * */
//params
$sort = optional_param('sort', 'title_of_training', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$tab = optional_param('tab', 'cpd_report', PARAM_TEXT);
$androgogic_activities_id = optional_param('androgogic_activities_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
if (isset($_POST['startdate'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']) - 1;
} else {
    $startdate = strtotime("-1 year", time());
}
if (isset($_POST['enddate'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'tab','androgogic_activities_id','user_id'));

//are we filtering on user?
if (!has_capability('block/androgogic_training_history:admin', $context)) {
    $user_id = $USER->id;
}

// prepare columns for results table
$columns = array(
    "title_of_training",
    "activity",
    "provider",
    "date_issued",
    "cpd_points",
);
if ($user_id == 0) {
    $columns[] = "user";
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
$and2 = '';

//are we filtering on androgogic_activities?
if ($androgogic_activities_id > 0) {
    $and .= " and mdl_androgogic_activities.id = $androgogic_activities_id ";
    //let's see which one they picked
    $chosen_activity = $DB->get_record('androgogic_activities', array('id' => $androgogic_activities_id));
    if ($chosen_activity->name != 'LMS course') {
        $and2 .= " and 1 = 0 ";
    }
}

if ($user_id > 0) {
    $and .= " and mdl_user.id = $user_id ";
    $and2 .= " and mdl_user.id = $user_id ";
}

if (isset($startdate)) {
    $and .= " and (a.date_issued > $startdate)";
    $and2 .= " and (cc.timecompleted > $startdate)";
}
if (isset($enddate)) {
    $and .= " and (a.date_issued < $enddate)";
    $and2 .= " and (cc.timecompleted < $enddate)";
}
if($CFG->androgogic_training_history_approval_workflow){
    $and .= " and a.approved = 1";
}
//had to do this fancy sequencing to allow both the join to work and for moodle's insistence on the first col being id and values being unique in it
$q = "SELECT row_number() over (ORDER BY $sort) as rownum, title_of_training, provider, date_issued, cpd_points, activity, USER FROM(
SELECT DISTINCT  a.title_of_training, a.provider, a.date_issued, a.cpd_points, mdl_androgogic_activities.name AS activity, mdl_user.firstname || ' ' || mdl_user.lastname AS USER 
FROM mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  ON a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_user  ON a.user_id = mdl_user.id
WHERE 1 = 1
$and
UNION
SELECT c.fullname AS title_of_training, '{$SITE->shortname}' AS provider, cc.timecompleted AS date_issued, d.data AS cpd_points, 'LMS course' AS activity, mdl_user.firstname || ' ' || mdl_user.lastname AS USER
FROM mdl_course_completions cc
INNER JOIN mdl_course c ON cc.course = c.id
INNER JOIN mdl_course_info_data d ON c.id = d.courseid
INNER JOIN mdl_course_info_field f ON d.fieldid = f.id
LEFT JOIN mdl_user ON cc.userid = mdl_user.id
WHERE STATUS >= 50
AND f.shortname = 'CPDpoints'
$and2
) b , (SELECT row_number() over () as rownum) r  
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
//also get the total number we have of these
$results_for_count = $DB->get_records_sql($q);
$result_count = count($results_for_count);

echo $OUTPUT->heading(get_string('cpd_report_heading', 'block_androgogic_training_history'));

if ($user_id > 0) {
    $subject_user = $DB->get_record('user', array('id' => $user_id));
    echo $OUTPUT->heading(fullname($subject_user));
    echo $OUTPUT->heading(fullname($subject_user->idnumber));
}

require_once('cpd_report_form.php');
$mform = new cpd_report_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'tab' => $currenttab, 'androgogic_activities_id' => $androgogic_activities_id, 'user_id' => $user_id, 'context' => $context));
$mform->display();

flush();

//RESULTS
if (!$results) {
    $match = array();
    echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history'));
} else {
    $table = new html_table();
    $table->head = array(
        $title_of_training,
        $activity,
        $provider,
        $date_issued,
        $cpd_points,
    );
    if ($user_id == 0) {
        $table->head[] = $user;
    }
    $table->align = array("left", "left", "left", "left", "left", "left", "left", "left", "left", "left", "left", "left",);
    $table->width = "95%";
    $table->size = array("8%", "8%", "8%", "8%", "8%", "8%", "8%", "8%", "8%", "8%", "8%", "8%",);
    $total_points = 0;
    foreach ($results as $result) {

        $td_array = array(
            "$result->title_of_training",
            "$result->activity",
            "$result->provider",
            date('Y-m-d', $result->date_issued),
            "$result->cpd_points",
        );
        if ($user_id == 0) {
            $td_array[] = $result->user;
        }
        $table->data[] = $td_array;
        $total_points += $result->cpd_points;
    }
    //total row
    $table->data[] = array(
        "<strong>Total</strong>",
        "",
        "",
        "",
        "<strong>$total_points</strong>",
    );
    if ($user_id == 0) {
        $table->data[] = "";
    }
}
if (!empty($table)) {
    echo html_writer::table($table);
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
}
?>
