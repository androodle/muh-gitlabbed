<?php

/**
 * Androgogic Training History Block: Install DB scripts
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     03/07/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_block_androgogic_training_history_install() {
    global $DB;
    $result = true;
    // create tables for the block
/*    if (!$DB->get_manager()->table_exists('androgogic_training_history')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history` (
            `id` bigint(11) NOT NULL AUTO_INCREMENT,
            `title_of_training` varchar(128) NOT NULL,
            `activity_id` int(10) DEFAULT NULL,
            `date_issued` bigint(20) NOT NULL,
            `provider` varchar(128) NOT NULL,
            `cpd_points` smallint(10) DEFAULT NULL,
            `file_id` bigint(10) DEFAULT NULL,
            `user_id` bigint(10) NOT NULL,
            `created_by` BIGINT(10) NOT NULL COMMENT 'relates to user table',
            `date_created` DATETIME NOT NULL,
            `modified_by` BIGINT(10) DEFAULT NULL COMMENT 'relates to user table',
            `date_modified` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
     if (!$DB->get_manager()->table_exists('androgogic_activities')) {
        $sql = "CREATE TABLE `mdl_androgogic_activities` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NOT NULL,
            `created_by` BIGINT(10) NOT NULL COMMENT 'relates to user table',
            `date_created` DATETIME NOT NULL,
            `modified_by` BIGINT(10) DEFAULT NULL COMMENT 'relates to user table',
            `date_modified` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }*/
    
    return $result;
}    
