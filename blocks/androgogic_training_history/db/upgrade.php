<?php

/** 
 * Androgogic Training History Block: Upgrade
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/
function xmldb_block_androgogic_training_history_upgrade($oldversion=0) {
global $CFG, $DB;
$result = true;

if ($DB->get_manager()->table_exists('androgogic_training_history')) {
     if (!$DB->get_manager()->field_exists('androgogic_training_history','approved')) {
         $sql = "ALTER TABLE `mdl_androgogic_training_history` ADD COLUMN `approved` TINYINT(1) DEFAULT 0 NULL AFTER `user_id`";
         $DB->execute($sql);
     }
     $sql = "ALTER TABLE `mdl_androgogic_training_history` CHANGE `cpd_points` `cpd_points` INT(10) NULL";
     $DB->execute($sql);
 } 

return $result;
}
// End of blocks/androgogic_training_history/db/upgrade.php
