<?php

/** 
 * Androgogic Training History Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     20/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the dimensions
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('andro_dimensions',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_training_history'), 'notifysuccess');

?>
