<?php

/** 
 * Androgogic Training History Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     20/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class dimension_edit_form extends moodleform {
protected $dimension;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_andro_dimensions a 
where a.id = {$_REQUEST['id']} ";
$dimension = $DB->get_record_sql($q);
}
else{
$dimension = $this->_customdata['$dimension']; // this contains the data of this form
}
$tab = 'dimension_new'; // from whence we were called
if (!empty($dimension->id)) {
$tab = 'dimension_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_training_history'), array('size'=>50));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');
//set values if we are in edit mode
if (!empty($dimension->id) && isset($_GET['id'])) {
$mform->setConstant('name', $dimension->name);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
