<?php

/** 
 * Androgogic Training History Block: Language Pack
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$string['pluginname'] = 'Androgogic Training History';
$string['plugintitle'] = 'Androgogic Training History';

//spares
//$string[''] = '';
//$string[''] = '';
//$string[''] = '';

//cron strings
$string['cron_hours'] = 'Cron hours';
$string['cron_hours_explanation'] = 'The hour at which the cron should execute in 24 hour format(0-23). Can be multiple: if so use semi-colon delimited list';
//approval_workflow
$string['approval_workflow'] = 'Require approval workflow';
$string['approval_workflow_explanation'] = 'If checked then the manager of the user will need to approve their training history records';
$string['approved'] = 'Approved';
$string['approved_story'] = 'If checked the training history record will show up in reports';

//training_history items
$string['training_history'] = 'Training History';
$string['title_of_training'] = 'Title Of Training';
$string['activity'] = 'Activity';
$string['date_issued'] = 'Date Issued';
$string['provider'] = 'Provider';
$string['cpd_points'] = 'Cpd Points';
$string['file'] = 'File';
$string['user'] = 'User';
$string['training_history_search'] = 'Training Histories';
$string['training_history_plural'] = 'Training Histories';
$string['training_history_new'] = 'New Training History';
$string['training_history_edit'] = 'Edit Training History';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_androgogic_training_history:admin'] = 'See and edit all histories within the Androgogic Training History block';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
$string['training_history_search_instructions'] = 'Search by Title Of Training or  Provider';
//activity items
$string['name'] = 'Name';
$string['activity_search'] = 'Activities';
$string['activity_plural'] = 'Activities';
$string['activity_new'] = 'New Activity';
$string['activity_edit'] = 'Edit Activity';
$string['activity_search_instructions'] = 'Search by Name';
$string['activity'] = 'Activity';
$string['editthisfile'] = 'Edit this file';
$string['upload'] = 'Upload';
$string['startdate'] = 'Date issued between: ';
$string['enddate'] = 'and';
$string['cpd_report'] = 'CPD report';
$string['cpd_report_heading'] = 'Continuing Professional Development (CPD) Achievement Report';

//approval emails
$string['email_to_manager_subject'] = 'Training history record approval request';
$string['email_to_manager_body'] = '<p>A Totara user for whom you are the manager, has requested your approval for a Training History record.</p>
    
<p>Please login to the Totara and approve it if you feel that it is valid.</p>

<p>{$a->url}</p>
';
$string['email_to_user_subject'] = 'Training history record has been approved';
$string['email_to_user_body'] = '<p>Congratulations,</p> 
    
<p>Your manager has approved your Training history record.</p>

<p>{$a->url}</p>
';
$string['userhasnomanager'] = 'Approval could not be sought for this training record, as the user has no manager';
$string['cantapproveowntraininghistory'] = 'You cannot approve your own training history record. However, the record has been saved, without the approval';