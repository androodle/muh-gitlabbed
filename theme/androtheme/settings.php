<?php

/**
 * Settings for the Androtheme
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_androtheme/logo';
        $title = get_string('logo','theme_androtheme');
        $description = get_string('logodesc', 'theme_androtheme');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Favicon file setting.
        $name = 'theme_androtheme/favicon';
        $title = new lang_string('favicon', 'theme_androtheme');
        $description = new lang_string('favicondesc', 'theme_androtheme');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Banner header text setting
        $name = 'theme_androtheme/bannertext';
        $title = get_string('bannertext', 'theme_androtheme');
        $description = get_string('bannertextdesc', 'theme_androtheme');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Link colour setting
        $name = 'theme_androtheme/linkcolor';
        $title = get_string('linkcolor','theme_androtheme');
        $description = get_string('linkcolordesc', 'theme_androtheme');
        $default = '#e88514';
        $previewconfig = array('selector'=>'a:link', 'style'=>'color');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Visited link colour setting
        $name = 'theme_androtheme/linkvisitedcolor';
        $title = get_string('linkvisitedcolor','theme_androtheme');
        $description = get_string('linkvisitedcolordesc', 'theme_androtheme');
        $default = '#e88514';
        $previewconfig = array('selector'=>'a:visited', 'style'=>'color');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Button colour setting
        $name = 'theme_androtheme/buttoncolor';
        $title = get_string('buttoncolor','theme_androtheme');
        $description = get_string('buttoncolordesc', 'theme_androtheme');
        $default = '#413d3c';
        $previewconfig = array('selector'=>'input[\'type=submit\']]', 'style'=>'background-color');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Page background colour setting
        $name = 'theme_androtheme/backgroundcolor';
        $title = get_string('backgroundcolor','theme_androtheme');
        $description = get_string('backgroundcolordesc', 'theme_androtheme');
        $default = '#ffffff';
        $previewconfig = array('selector'=>'body', 'style'=>'backgroundColor');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Header background colour setting
        $name = 'theme_androtheme/headerbgcolor';
        $title = get_string('headerbgcolor','theme_androtheme');
        $description = get_string('headerbgcolordesc', 'theme_androtheme');
        $default = '#413d3c';
        $previewconfig = array('selector'=>'#page-header', 'style'=>'background');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Page-nav (Breadcrumb) text colour setting
        $name = 'theme_androtheme/pagenavtextcolor';
        $title = get_string('pagenavtextcolor','theme_androtheme');
        $description = get_string('pagenavtextcolordesc', 'theme_androtheme');
        $default = '#000000';
        $previewconfig = array('selector'=>'.breadcrumb', 'style'=>'color');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Page-nav (Breadcrumb) background colour setting
        $name = 'theme_androtheme/pagenavbgcolor';
        $title = get_string('pagenavbgcolor','theme_androtheme');
        $description = get_string('pagenavbgcolordesc', 'theme_androtheme');
        $default = '#dfdfdf';
        $previewconfig = array('selector'=>'#page-navbar', 'style'=>'background');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Block header background color setting
        $name = 'theme_androtheme/blockheaderbgcolor';
        $title = get_string('blockheaderbgcolor','theme_androtheme');
        $description = get_string('blockheaderbgcolordesc', 'theme_androtheme');
        $default = '#413d3c';
        $previewconfig = array('selector'=>'.block .header', 'style'=>'background');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Block background color setting
        $name = 'theme_androtheme/blockbgcolor';
        $title = get_string('blockbgcolor','theme_androtheme');
        $description = get_string('blockbgcolordesc', 'theme_androtheme');
        $default = '#ffffff';
        $previewconfig = array('selector'=>'.block .content', 'style'=>'background');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Block header text color setting
        $name = 'theme_androtheme/blockheadertextcolor';
        $title = get_string('blockheadertextcolor','theme_androtheme');
        $description = get_string('blockheadertextcolordesc', 'theme_androtheme');
        $default = '#ffffff';
        $previewconfig = array('selector'=>'.block .header h2', 'style'=>'color');
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Custom CSS file
        $name = 'theme_androtheme/customcss';
        $title = get_string('customcss', 'theme_androtheme');
        $description = get_string('customcssdesc', 'theme_androtheme');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_androtheme/footnote';
        $title = get_string('footnote', 'theme_androtheme');
        $description = get_string('footnotedesc', 'theme_androtheme');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

}