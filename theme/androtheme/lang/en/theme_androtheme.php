<?php

$string['choosereadme'] = '
<div class="clearfix">
        <div class="theme_screenshot">
                <h2>AndroTheme</h2>
                <img src="androtheme/pix/screenshot.jpg" />
                <h3>Theme Credits</h3>
                <p>Created by Androgogic</p>
                <h3>Report a bug:</h3>
                <p><a href="https://androgogic.livetime.com/" target="_blank">https://androgogic.livetime.com/</a></p>
        </div>
        <div class="theme_description">
                <h2>About</h2>
                <p>AndroTheme is a flexible responsive theme</p>
                <h2>Tweaks</h2>
                <p>This theme is built upon the Bootstrap theme inside the Moodle core.</p>
        </div>
</div>
';

$string['pluginname'] = 'AndroTheme';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here.';

$string['favicon'] = 'Favicon';
$string['favicondesc'] = 'Select or upload the image file to be used as the site\'s favicon, the icon must be *.ico format';

$string['bannertext'] = 'Additional banner text (optional)';
$string['bannertextdesc'] = 'This text goes to the right hand side of the logo.';

/* Colors */
$string['headerbgcolor'] = 'Header background colour';
$string['headerbgcolordesc'] = 'This sets the background colour of the main heading block.';

$string['backgroundcolor'] = 'Background page colour';
$string['backgroundcolordesc'] = 'This sets the background colour of the page.';

$string['pagenavtextcolor'] = 'Breadcrumb (Page Navigation) text colour';
$string['pagenavtextcolordesc'] = 'This sets the text colour of the breadcrumb (page navigation) bar.';

$string['pagenavbgcolor'] = 'Breadcrumb (Page Navigation) background colour';
$string['pagenavbgcolordesc'] = 'This sets the background colour of the breadcrumb (page navigation) bar.';

$string['blockheadertextcolor'] = 'Block header text colour';
$string['blockheadertextcolordesc'] = 'This sets the text colour of the block\'s header.';

$string['blockheaderbgcolor'] = 'Block header background colour';
$string['blockheaderbgcolordesc'] = 'This sets the background colour of the block\'s header.';

$string['blockbgcolor'] = 'Block background colour';
$string['blockbgcolordesc'] = 'This sets the background colour of the block\'s content.';

$string['linkcolor'] = 'Link color';
$string['linkcolordesc'] = 'This sets the link color for the theme.';

$string['linkvisitedcolor'] = 'Visited link color';
$string['linkvisitedcolordesc'] = 'This sets the link color for visited links.';

$string['buttoncolor'] = 'Button color';
$string['buttoncolordesc'] = 'This sets the color for the background of buttons.';

/* Custom CSS */

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Any CSS you enter here will be added to every page allowing your to easily customise this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'The content from this textarea will be displayed in the footer of every page.';

$string['region-side-post'] = 'Right';
$string['region-content'] = 'Middle';
$string['region-side-pre'] = 'Left';